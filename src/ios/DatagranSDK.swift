//
//  DatagranSDK.swift
//  Datagran
//
//  Created by Karthikeyan K .
//  Copyright (c) Datagran. All rights reserved.
//

import datagran_iOS_sdk

//Constant to check geo location is enabled
var addGeo = false
//Constant to check appKey and workspaceID is available
var initiated = false

//Constants to store config.xml value
let datagranAppKey = "DatagranAppKey"
let datagranWorkspaceId = "DatagranWorkspaceId"
let datagranBatchValue = "DatagranBatchValue"
let datagranWaitResponse = "DatagranWaitResponse"

@objc(DatagranSDK) class DatagranSDK : CDVPlugin {
    
    //This method is used to initialise datagran framework
    override func pluginInitialize() {
       // Init Method
    }
    
    @objc(initSdk:)
    func initSdk(command: CDVInvokedUrlCommand) {
        let jsonData = command.argument(at: 0)
        let jsonDictionary = jsonData as? Dictionary<String,AnyObject> ?? [:]
        if !jsonDictionary.isEmpty{
            let appKey = jsonDictionary["datagranAppKey"] as? String ?? ""
            let workSpace = jsonDictionary["datagranWorkspaceId"] as? String ?? ""
            var batch:Int = jsonDictionary["datagranBatchValue"] as! Int
            var waitResponse = jsonDictionary["datagranWaitResponse"] as! Int
            let batchValue = commandDelegate.settings[datagranBatchValue.lowercased()] as? String ?? ""
            if batchValue != ""{
                batch = Int(batchValue) ?? 0
            }
            let waitResponseValue = commandDelegate.settings[datagranWaitResponse.lowercased()] as? String ?? ""
            if waitResponseValue != ""{
                waitResponse = Int(waitResponseValue) ?? 0
            }
            
            if appKey != "" && workSpace != ""{
                if batch != 0 && waitResponse != 0{
                    Tracker.shared.initialize(appKey, showLog: true, addGeo: addGeo, sendBatch: TimeInterval(batch), waitForResponse: TimeInterval(waitResponse),workSpaceId: workSpace)
                    
                }else if batch > 0 && waitResponse == 0{
                    Tracker.shared.initialize(appKey, showLog: true, addGeo: addGeo, sendBatch: TimeInterval(batch),workSpaceId: workSpace)
                }else{
                    Tracker.shared.initialize(appKey, showLog: true, addGeo: addGeo, waitForResponse: TimeInterval(waitResponse),workSpaceId: workSpace)
                }
                
                Tracker.shared.CreateDGuserid()
                initiated = true
            } else {
                print("Appkey and workspace not found")
                return
            }
            
        } else {
            
            print("Datagran SDK not initialized with appKey and WorkspaceID or Invalid parameter")

        }
        
    }
    
    // This method is used to call the identify method in datagtan framework
    @objc(identify:) // Declare your function name.
    func identify(command: CDVInvokedUrlCommand) {
        var pluginResult: CDVPluginResult
        let idValue = command.arguments[0] as? String ?? ""
        
        if initiated && idValue != ""{
            Tracker.shared.identify(id: idValue, controller: self.viewController, addGeo: addGeo)
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Identify method successfully invoked !!!");
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }else{
            pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "Datagran SDK not initialized with appKey and WorkspaceID or Invalid parameter");
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }
    }
    
    // This method is used to call the resetDGuserid method in datagtan framework
    @objc(resetDGuserid:) // Declare your function name.
    func resetDGuserid(command: CDVInvokedUrlCommand) {
        var pluginResult: CDVPluginResult
        if initiated{
            Tracker.shared.resetDGuserid()
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "resetDGuserid method successfully invoked !!!");
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }else{
            pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "Datagran SDK not initialized with appKey and WorkspaceID");
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }
    }
    
    
    // This method is used to call the trackCustom method in datagtan framework
    @objc(trackCustom:) // Declare your function name.
    func trackCustom(command: CDVInvokedUrlCommand) {
        var pluginResult: CDVPluginResult
        let jsonData = command.argument(at: 0)
        var jsonDictionary = jsonData as? Dictionary<String,AnyObject> ?? [:]
        if initiated && !jsonDictionary.isEmpty{
            let eventName = jsonDictionary["eventName"]
            if eventName == nil{
                pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "Invalid parameters");
            }else{
                jsonDictionary.removeValue(forKey: "eventName")
                Tracker.shared.trackCustom(viewName: eventName as! String, action: jsonDictionary, controller: self.viewController, addGeo: addGeo)
                pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "Event has been successfully passed !!!");
            }
        }else{
            pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "Datagran SDK not initialized with appKey and WorkspaceID");
            self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
        }
    }
    
    // This method is used to add geo location
    @objc(updateGeoParam:) // Declare your function name.
    func updateGeoParam(command: CDVInvokedUrlCommand) {
        var pluginResult: CDVPluginResult
        let idValue = command.arguments[0] as? String ?? ""
        if idValue != "" {
            addGeo = (idValue == "on")
            pluginResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "updateGeoParam method successfully invoked !!!");
        }else{
            pluginResult = CDVPluginResult (status: CDVCommandStatus_ERROR, messageAs: "Invalid parameters");
        }
        self.commandDelegate!.send(pluginResult, callbackId: command.callbackId);
    }
}






